# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:/OpJKWupvFd8WJX1mTt8vi01pP7dkA6e//4l4C3TExE=",
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "h1:KtUCltnScfZbcvpE9wPH+a0e7KgMX4w7y8RSxu5J/NQ=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "h1:12Bac8B6Aq2+18xe8iqp5iYytav2Bw+jG43z/VaK5zI=",
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "h1:LN84cu+BZpVRvYlCzrbPfCRDaIelSyEx/W9Iwwgbnn4=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.32.1"
  constraints = ">= 1.32.0"
  hashes = [
    "h1:RfLu8m+y3fKf5FrDJarSp06KS4R75yZxPy9n+Df5PjM=",
    "h1:oYtGxm7gv9NClz0kfEq3KtlGiutkK9SmvYNGeJLITg4=",
    "h1:ozet1RE3PXUTi2fWeUnrlZqJypvwHA+noNZBZumV1S4=",
    "h1:scywMnv4OEq37tG5uiPLaCfYtcZpAsikCcLZuapbvhY=",
    "zh:043e941caf46b3a37cae5f2f9c1b7ce2b30f0b492bada6f3d8d6a7384f5cb7b2",
    "zh:055835d483dd172e7b0e500f9dd789353e32c9328d51793b8d88451df4e92067",
    "zh:3dd5a0006ab7f464a2bca8c5a46e583c6f09ba66aeff1ca847397b61fc823597",
    "zh:3f0444956fdcb059ee9ea54f51af016d86f297477335f519256ca158a75c5e59",
    "zh:569a80f0c9e2f5fb121d9050bc10d6e6ba30507e2e985b809a2613dcb5bdc095",
    "zh:5e7e8499e62408d784d4c886827d421962134a3efda5f5f4f8794f9b1c17190c",
    "zh:67b48380e144ba4c31fff41442cbf53463eba285321d4283430b605285048923",
    "zh:7ddc434dbefecc6b1934f683f54ad5552c9e466b5e256b9cfe67f7b28ffecc7d",
    "zh:87c0b5f4f6b3121cc81935ccb8598a58bda20c7f96f8a4270ecb0b6b2096ba40",
    "zh:891d2234146c3fbc2fe6d2a0c176cefd01d16d2d1d25eebe6e15909aac4a1ddf",
    "zh:a90ced7f84d8bdd64afd00c69ea9e2b1ed43314da020860c31ff266b3716d1f0",
    "zh:b8c86266d9f4ae4d2cca8f3f7d58a48d0c000f16aa21a733bb81c760efa690f7",
    "zh:bf8fdd6eb8619dc20d85d418ed910a79af0b28bf79ac3a4029f0d0ae032c9c7d",
    "zh:d6d87b405c0fd5e576a7e0a8976689d555299806484fdacca205537367d92f37",
  ]
}
