# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version     = "2.1.0"
  constraints = ">= 2.1.0"
  hashes = [
    "h1:/OpJKWupvFd8WJX1mTt8vi01pP7dkA6e//4l4C3TExE=",
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "h1:KtUCltnScfZbcvpE9wPH+a0e7KgMX4w7y8RSxu5J/NQ=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "1.23.0"
  constraints = ">= 1.23.0"
  hashes = [
    "h1:0UZh5QINwSSZkVKD1Qhe08LkzJr48bZBQowpELRHDv8=",
    "h1:GxaW8tI4G9dkMrOJjE5Bu1s2stZHGazq0a5yW6ren64=",
    "h1:KArDOaKRxAPVNU0TxVp4XfJshSye7tWUyzcu94CwOsY=",
    "h1:qAWYaCuVsJNnQisJL6mMHKga6A8yxwX8N8wXbr3RO2g=",
    "zh:070c7f448c855cf5d8158dee40adbf424e43ab9207df74a3598f4d645cce3934",
    "zh:0c0e37246c35afd8429f22b1ca8403a06968681f1c8ae1137543a2e99bb2f8ab",
    "zh:2c73180e578d26feea39450edd7b51395ecc21d3acc9b750e34c1c349e5068b3",
    "zh:35a430142d16c113c82bd36f22fccc621554c8f8a8e0e871523cd9815d1865fe",
    "zh:460273e7afb08545e9e2b71dee259140c084ed0e411b4ff77fa418794f3d8f43",
    "zh:65cf357e078194c0b920ff25136276f655c289b45699a6f5eadd85954a9c7795",
    "zh:66cac3e5797ee2afe620e9753a1e2631bcc151f25a65a3012f221c29b868bce6",
    "zh:785049ae99c7521d78bef08ed7edbc1c972c76ff8f0830734c5da88560f384a1",
    "zh:8195ff649d6da65c3d3dbb2279706d3e8ae8f11c1eb0127a322afe881894b33f",
    "zh:8fb55e659305a8c31d83bf627ff6f7e3525643bac99a5ae907e974085d4da24c",
    "zh:a33ffef16c027b6a451fc5aeabd49790c9ebf35b978b21382ae5b721cda6883d",
    "zh:da1c77a8f89d3ff4e7347dc7ad32b7c5e0a6cb7159e187b594396072fd0772b6",
    "zh:f0198bb497853f05d0c51361b46b8a3b45ecc2d9808905382325df5ee237a186",
    "zh:f99e479106e7bc43da7ef0b69e465d6b347d91217cda73b6196377811c5fd23b",
  ]
}
