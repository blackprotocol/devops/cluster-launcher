# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.15.0"
  constraints = ">= 2.15.0"
  hashes = [
    "h1:+RuxNOXdPNo0hwgXFyoYo1luSQ7LSB7CC+tVCcz0hz8=",
    "h1:98Hoeh5ulD2gXbaWFAmqewFo2HKquTsiXj8CBdhajsg=",
    "h1:W4fNSeg4CVfir2vIs9H7mcKFk5lOUxbmOMwGbfl2ZjM=",
    "h1:gdaK4qhuw6C6Q1qNTZFtlXo5rZVO5EPiL4ZRM8z3k2I=",
    "zh:3398034380c1cf0c9439dfeac857889aa47c60b75198cc14a238d787c6de28f8",
    "zh:4bd4314f4c0b8f196746f9507d5a919d06a1df9aeca6c4366ed7d2de44d6b1b4",
    "zh:5c08823e87825fe6043b85780e5bccc2a746549fbcee6e157dcfab109865ffe0",
    "zh:6d03f81ee4d0bc30910df3ede1e0776a779d04488d3e0fd6266a2baa641e29ad",
    "zh:73d899c1c297da784be78da3865230db8c538c4e979b3154dbb8af9882a307e1",
    "zh:7c1eeb131ad168d4322804d6263ad721970b78d1e4c83ad29a6130726e149215",
    "zh:7d3e0312c0cf76630f67b94e2c85862e8f96f31945080514df103d24cbd66983",
    "zh:7daedcfeb453d3d5f17eece2e77d1969730d76310ee7d787c931d64928148510",
    "zh:8729246056c7ddade5c5e37540a7e6920bc6f6d9129901c7984461f4243e79d5",
    "zh:8a0d54890299e666420b7967d3746cc93f9485909c50b0e92fc1d90fafeec58d",
    "zh:9ac83aeb960198edf20c6affa70c1ee3d07f0157d9764200322efba4d7a01555",
    "zh:b2af55740c5c214a758598cac8714e5d34d8160d20096ba1d807901dc6cbd54f",
    "zh:d85951333327532e921dae4bdedb1917caaf5db8916b4a049e5414b029dfb5a7",
    "zh:dbda132523eb744d958f82aee49e8f64ae357aa8bd9a902d265c62eb4bead64d",
    "zh:e83d45d404be7e712eab816c2fc384f25674c6f9a90fa7c41d3cdaeee3160880",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version     = "2.1.0"
  constraints = ">= 2.1.0"
  hashes = [
    "h1:/OpJKWupvFd8WJX1mTt8vi01pP7dkA6e//4l4C3TExE=",
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "h1:KtUCltnScfZbcvpE9wPH+a0e7KgMX4w7y8RSxu5J/NQ=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}
